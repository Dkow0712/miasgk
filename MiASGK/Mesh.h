#pragma once
#include <vector>
#include "Int3.h"
#include "Vertex.h"
#include "OBJLoader.h"
#include "VertexProcessor.h"
#include "Rasterizer.h"
#include "Light.h"

class Mesh
{
public:
	Mesh();
	~Mesh();

	bool loadMesh(std::string fileName);
	void drawMesh(Rasterizer& rasterizer, VertexProcessor& vp, Light& light);
	void drawMesh(Rasterizer& rasterizer, VertexProcessor& vp, std::vector<Light*> lights);

	int vSize;
	int tSize;

	OBJLoader objLoader;

	std::vector<Vertex> vertices;
	std::vector<Int3> indices;
};

