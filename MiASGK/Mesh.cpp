#include "Mesh.h"

Mesh::Mesh()
	:objLoader()
{
}

bool Mesh::loadMesh(std::string fileName)
{
	bool isSuccess = this->objLoader.loadRawModel(fileName);
	if (isSuccess)
	{
		this->vSize = this->objLoader.vertices.size();
		this->tSize = this->objLoader.indices.size() / 3;
		//std::cout << vSize << " " << tSize;
		this->vertices = this->objLoader.vertices;
		this->indices = this->objLoader.indices;
		return true;
	}
	
	return false;
}

void Mesh::drawMesh(Rasterizer& rasterizer, VertexProcessor& vp, Light& light)
{

	for (int i = 0; i < this->tSize; i++) //TODO
	{
		Float3 colorA = light.calculate(vertices[i * 3], vp);
		Float3 colorB = light.calculate(vertices[i * 3 + 1], vp);
		Float3 colorC = light.calculate(vertices[i * 3 + 2], vp);
		//std::cout << i << std::endl;
		rasterizer.triangle(
	/*		vp.tr(vertices[(this->indices[i].a - 1)].position),
			vp.tr(vertices[this->indices[i+1].a - 1].position),
			vp.tr(vertices[this->indices[i+2].a - 1].position)*/
			vp.tr(vertices[i*3].position),
			vp.tr(vertices[i*3+1].position),
			vp.tr(vertices[i*3+2].position),
			colorA,
			colorB, 
			colorC
		);
	}
}

void Mesh::drawMesh(Rasterizer& rasterizer, VertexProcessor& vp, std::vector<Light*> lights)
{

	for (int i = 0; i < this->tSize; i++) //TODO
	{
		Float3 colorA = Float3();
		Float3 colorB = Float3();
		Float3 colorC = Float3();
		for (auto light : lights)
		{
			colorA = colorA + light->calculate(vertices[i * 3], vp);
			colorB = colorB + light->calculate(vertices[i * 3 + 1], vp);
			colorC = colorC + light->calculate(vertices[i * 3 + 2], vp);
		}
		colorA = colorA.Saturate();
		colorB = colorB.Saturate();
		colorC = colorC.Saturate();
		//std::cout << i << std::endl;
		rasterizer.triangle(
			/*		vp.tr(vertices[(this->indices[i].a - 1)].position),
					vp.tr(vertices[this->indices[i+1].a - 1].position),
					vp.tr(vertices[this->indices[i+2].a - 1].position)*/
			vp.tr(vertices[i * 3].position),
			vp.tr(vertices[i * 3 + 1].position),
			vp.tr(vertices[i * 3 + 2].position),
			colorA,
			colorB,
			colorC
		);
	}
}

Mesh::~Mesh()
{
}
