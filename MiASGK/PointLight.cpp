#include "PointLight.h"



PointLight::PointLight(Float3 position, Float3 ambient, Float3 diffuse, Float3 specular, float shininess)
	: Light(position, ambient, diffuse, specular, shininess)
{
}

PointLight::~PointLight()
{
}

Float3 PointLight::calculate(Fragment & f, VertexProcessor & vp)
{
	Float3 p = position;// .Normalized();

	Float3 n = vp.obj2View(f.normal);

	n = n.Normalized();

	Float4 v4 = vp.GetObj2ViewMatrix().mulByFloat3(!f.position);

	Float3 v = Float3(v4.x, v4.y, v4.z);

	v = v.Normalized();

	Float3 l = (p - v);

	l = l.Normalized();

	Float3 r = l.Reflect(l, n);

	r = r.Normalized();

	Float3 diffusePart = clamp(l.Dot(n), 0, 1) * diffuse;

	Float3 specularPart = (float)std::pow(clamp(r.Dot(v), 0, 1), shininess) * specular;

	Float3 result = (ambient + diffusePart) + specularPart;

	if (result.x > 255)
	{
		result.x = 255;
	}

	if (result.y > 255)
	{
		result.y = 255;
	}

	if (result.z > 255)
	{
		result.z = 255;
	}

	return result;
}
