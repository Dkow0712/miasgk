#include <iostream>
#include <memory>
#include <vector>

#include "WriteBufferInterface.h"
#include "PPMBuffer.h"
#include "Rasterizer.h"
#include "Float4.h"
#include "Float4x4.h"
#include "VertexProcessor.h"
#include "Mesh.h"
#include "DirectionalLight.h"
#include "PointLight.h"

void testTriangle();
void testMath();

int main()
{
	//testMath();
	testTriangle();
	//testObjLoader();
	std::getchar();
	return 0;
}

void testTriangle()
{
	VertexProcessor vp;
	vp.setPerspective(45, 9 / 6.0f, 1, 100);
	vp.setIdentityWorld2View();
	//vp.setLookAt(Float3(0, 0, 4), Float3(0, 0, 1), Float3(0, 1, 0));
	vp.setLookAt(Float3(0, 0, -15), Float3(-4, 0, 0), Float3(0, 1, 0));
	vp.setIdentityObj2World();

	//vp.multByTranslation(Float3(2,2,2));
	vp.multByScale(Float3(4, 4, 4));
	//vp.multByRotation(0, Float3(0, 1, 0));

	//vp.multByRotation(120, Float3(1, 0, 0));

	vp.transform();
	std::shared_ptr<WriteBufferInterface> savePPMBuffer = std::make_shared<PPMBuffer>();
	std::shared_ptr<ColorBuffer> colorBuffer = std::make_shared<ColorBuffer>(512, 512);
	colorBuffer->setUniformColor(0xff8300);
	Rasterizer rasterizer(colorBuffer); //przeciwnie do ruchu wskaz�wek zegara
	Float3 a(-0.5f, 0.0f, 0.0f);
	Float3 b(1.0f, 1.0f, 0.0f);
	Float3 c(1.0f, 0.0f, 0.0f);
	Float3 a1(-0.5f, -0.5f, -0.5f);
	Float3 b1(0.0f, 0.5f, 0.5f);
	Float3 c1(0.5f, -0.5f, 0.5f);
	//rasterizer.triangle(vp.tr(a), vp.tr(b), vp.tr(c));
	//vp.multByTranslation(Float3(0.4, 0.4, 0.4));
	//vp.multByRotation(90, Float3(0, 0, 1));
	//vp.transform();
	//rasterizer.triangle(vp.tr(a), vp.tr(b), vp.tr(c));
	Mesh mesh;
	mesh.loadMesh("sphere.obj");
	DirectionalLight* light1 = new DirectionalLight(Float3(-1.0f, 0.0f, 0.0f), Float3(10.0f, 10.0f, 10.0f), Float3(255.0f, 255.0f, 255.0f), Float3(0.0f, 0.0f, 0.0f), 10.0f);
	DirectionalLight* light2 = new DirectionalLight(Float3(1.0f, 0.0f, 0.0f), Float3(10.0f, 10.0f, 10.0f), Float3(255.0f, 255.0f, 255.0f), Float3(0.0f, 0.0f, 0.0f), 10.0f);
	std::vector<Light*> lights;
	lights
.push_back(light1);
	lights.push_back(light2);
	//PointLight light = PointLight(Float3(7.0f, 0.0f, 5.0f), Float3(10.0f, 10.0f, 10.0f), Float3(0.0f, 0.0f, 200.0f), Float3(200.0f, 200.0f, 200.0f), 10.0f);
	mesh.drawMesh(rasterizer, vp, lights);
	//rasterizer.triangle(a1, b1, c1, Float3(244.0f, 232.0f, 66.0f), Float3(68.0f, 244.0f, 235.0f), Float3(238.0f, 10.0f, 255.0f));
	savePPMBuffer->saveToFile("test.ppm", colorBuffer);

	std::cout << "Koniec" << std::endl;

	delete light1;
	delete light2;
}

void testMath()
{
	//TODO: sprawdzi� czy dzia�a Float4

	Float4 a(1, 5, 8, 5);
	Float4 b(2, 5, 5, 8);
	Float4 c(3, 5, 8, 5);
	Float4 d(4, 5, 5, 8);
	Float4x4 m1(a, b, c, d);

	Float3 ee(5, 50, 45);
	Float4 e(5, 50, 45, 2);
	Float4 f(4, 5, 65, 8);
	Float4 g(2, 1, 68, 66);
	Float4 h(0, 9, 8, 78);
	Float4x4 m2(e, f, g, h);

	Float4x4 res = m1.mulByFloat4x4(m2);
	Float4 resFloat4 = m1.mulByFloat4(e);
	Float4 resFloat3 = m1.mulByFloat3(ee);

	m1.Display();
	std::cout << std::endl;
	resFloat3.Display();
	//m2.Display();
	std::cout << std::endl;
	//res.Display();
	//Float4x4 tran = res.transpose();
	//std::cout << std::endl;
	//tran.Display();
}